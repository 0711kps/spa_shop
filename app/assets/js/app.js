import '../css/app.sass'
import React from 'react';
import ReactDOM from 'react-dom';
import Fetch from './components/Fetch'

class App extends React.Component {
  render() {
    return <Fetch />
  }
}

ReactDOM.render(
  <App />, document.getElementById('container')
);
