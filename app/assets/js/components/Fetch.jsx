import React from  'react'
import Main from './Main'

class Fetch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rsobj: {},
      isLoaded: false,
      error: null
    }
  }

  componentDidMount() {
    fetch('/retrieve_items')
      .then(res => res.json())
      .then((result) => {
        this.setState({
          isLoaded: true,
          rsobj: result
        })
      }, error => {
        this.setState({
          isLoaded: true,
          error: error
        })
      })
  }

  render() {
    const { isLoaded, error, rsobj } = this.state
    if (error) {
      return <div>Error: { error.message }</div>
    } else if (!isLoaded) {
      return <div id='loading'>Loading...</div>
    } else {
      return (
        <Main items={rsobj.items} page={rsobj.page} />
      )
    }
  }
}

export default Fetch
