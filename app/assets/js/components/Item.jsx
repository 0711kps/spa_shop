import React from 'react'

class Item extends React.Component {
  render() {
    return (
      <article className='item'>
        <p>品名: {this.props.title}</p>
        <p>數量: {this.props.inventory}</p>
        <input className='amount' type='number' min='0' />
        <input className='buy' type='checkbox' />
      </article>
    )
  }
}

export default Item
