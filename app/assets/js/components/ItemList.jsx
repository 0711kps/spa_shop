import React from 'react'
import Item from './Item'

class ItemList extends React.Component {
  render() {
    let currentPage = this.props.currentPage
    let items = this.props.items.slice((currentPage - 1) * 4, currentPage * 4)
    return (
      items.map(item => <Item title={item.title} inventory={item.inventory} />)
    )
  }
}

export default ItemList
