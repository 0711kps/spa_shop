import React from 'react'
import ItemList from './ItemList.jsx'
import Paginator from './Paginator'
import { Switch, HashRouter, Route, Redirect, Link } from 'react-router-dom'

class Main extends React.Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route
            path="/items/:page"
            render={props => (
              <div id='wrapper'>
                <ItemList items={this.props.items} currentPage={props.match.params.page} />
                <Paginator max={this.props.page.max} current={props.match.params.page} />
              </div>
            )}
          />
          <Redirect from="" to="/items/1" />
        </Switch>
      </HashRouter>
    )
  }
}

export default Main
