import React from 'react'
import { Link } from 'react-router-dom'

class Paginator extends React.Component {
  render() {
    return (
      <div id="paginator">
        {
          [...new Array(this.props.max).keys()]
            .map(p => p + 1)
            .map(p => {
              if (p === parseInt(this.props.current)) {
                return <span>{p}</span>
              } else {
                return <Link to={"/items/" + p}>{p}</Link>
              }
            })
        }
      </div>
    )
  }
}

export default Paginator
