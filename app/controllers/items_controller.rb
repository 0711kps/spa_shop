class ItemsController < ApplicationController
  def index
    # empty for just render container
  end

  def retrieve_items
    @items = Item.all
    render json: {
      items: @items,
      orders: [],
      page: {
        max: (@items.length / 4.0).ceil,
        current: 1
      }
    }
  end
end
