class Item < ApplicationRecord
  has_many :orders
  has_many :customer_infos, through: :orders
  validate :legal?

  def currently_available?
    inventory.positive?
  end

  private

  def legal?
    errors.add(:inventory, 'cannot be negative') if inventory.negative?
  end
end
