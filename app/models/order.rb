class Order < ApplicationRecord
  belongs_to :item
  belongs_to :order_list
  validates :order_list_id, presence: true
  validates :item_id, presence: true
  validates :amount, numericality: { greater_than: 0 }
  validate :inventory_enough?, if: :item_id
  after_save :reduce_inventory

  def inventory_enough?
    return if item.inventory >= amount

    errors.add(:amount, 'cannot be more than item inventory')
  end

  def reduce_inventory
    item.update(inventory: item.inventory - amount)
  end
end
