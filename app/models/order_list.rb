class OrderList < ApplicationRecord
  has_many :orders
  validates :client, presence: true,
                     length: { minimum: 1, maximum: 30 }
  validates :email, presence: true,
                    length: { minimum: 6, maximum: 180 }
  validates_format_of :email,
                      with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :address, presence: true,
                      length: { minimum: 2, maximum: 60 }

  # return order_list or false
  def self.build_with(params)
    transaction do
      order_list = new(params[:list_info])
      return false unless order_list.save

      return false unless params[:order_info].all? do |info|
        order = order_list.orders.new(info)
        order.save
      end

      order_list.orders.empty? ? false : order_list
    end
  end

  private_class_method :new
end
