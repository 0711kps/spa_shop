Rails.application.routes.draw do
  root to: 'items#index'
  get '/retrieve_items', to: 'items#retrieve_items'
end
