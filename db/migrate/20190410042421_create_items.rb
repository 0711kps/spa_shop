class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :title, null: false
      t.integer :inventory, default: 0

      t.timestamps
    end
  end
end
