class CreateOrders < ActiveRecord::Migration[5.2]
  def up
    create_table :orders do |t|
      t.integer :item_id, null: false
      t.integer :order_list_id, null: false
      t.integer :amount, null: false

      t.timestamps
    end
  end

  def down
    drop_table :orders
  end
end
