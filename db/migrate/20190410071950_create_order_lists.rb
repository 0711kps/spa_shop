class CreateOrderLists < ActiveRecord::Migration[5.2]
  def up
    create_table :order_lists do |t|
      t.string :client
      t.string :email
      t.string :address

      t.timestamps
    end
  end

  def down
    drop_table :order_lists
  end
end
