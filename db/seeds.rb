# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
items = %w(
  限量抹茶紅豆銅鑼燒-採用宇治抹茶
  培根雞蛋麵-麵條採用意大利杜蘭朵小麥
  韓式炸雞
  栗子蒙布朗
  草莓布朗尼
  蟹肉蒸餃
  柿子果醬
  日本高知縣產夢幻番茄「復興番茄」
  日本宮崎縣產超甜A級水果玉米「黃金豐饒」
  芒果生乳酪蛋糕
)

items.each do |item|
  Item.create(title: item, inventory: rand(3..99))
end
