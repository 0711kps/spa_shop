require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  test 'cannot save with negative inventory' do
    pasta = Item.new(title: '培根雞蛋麵', inventory: -10)
    assert_equal pasta.save, false
  end

  test 'return boolean for empty inventory' do
    rare_snack = Item.new(title: '限量抹茶紅豆銅鑼燒')
    assert_equal rare_snack.currently_available?, false
  end
end
