require 'test_helper'

class OrderListTest < ActiveSupport::TestCase
  test 'OrderList need at least one order' do
    ol = OrderList.build_with(
      list_info: {
        client: '老王',
        email: 'wang@mail.com',
        address: 'next to you'
      },
      order_info: []
    )
    assert_equal ol, false
  end

  test 'all orders in OrderList must be legal' do
    ol = OrderList.build_with(
      list_info: {
        client: '老王',
        email: 'wang@mail.com',
        address: 'next to you'
      },
      order_info: [
        { item_id: Item.first, amount: -1 },
        { item_id: Item.last, amount: 99 }
      ]
    )
    assert_equal ol, false
  end
end
