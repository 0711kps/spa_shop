require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test 'cannot save without order_list relation' do
    order = Order.new(item: Item.first, amount: 1)
    assert_equal order.save, false
  end

  test 'cannot save without item' do
    order = Order.new(amount: 1, order_list: OrderList.first)
    assert_equal order.save, false
  end

  test 'cannot save without amount' do
    order = Order.new(amount: 0, order_list: OrderList.first)
    assert_equal order.save, false
  end

  test 'amount cannot be greater than inventory' do
    order = Order.new(item: Item.last, order_list: OrderList.first, amount: 999)
    assert_equal order.save, false
  end
end
