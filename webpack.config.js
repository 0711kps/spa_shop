const path = require('path')

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.sass/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.sass', '.jsx']
  },
  entry: './app/assets/js/app.js',
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'main.js'
  }
}
